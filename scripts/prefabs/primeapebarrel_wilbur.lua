local assets =
{
    Asset("ANIM", "anim/monkey_barrel_wilbur.zip"),
    Asset("SOUND", "sound/monkey.fsb"),
}

local prefabs =
{
    "primeape",
    "poop",
    "cave_banana",
    "collapse_small",
}

local function onhammered(inst)
    if inst.shake ~= nil then
        inst.shake:Cancel()
        inst.shake = nil
    end
    inst.components.lootdropper:DropLoot()
    local fx = SpawnPrefab("collapse_small")
    fx.Transform:SetPosition(inst.Transform:GetWorldPosition())
    fx:SetMaterial("wood")
    inst:Remove()
end

local function onhit(inst, worker)
    if inst.components.childspawner ~= nil then
        inst.components.childspawner:ReleaseAllChildren(worker)
    end
    inst.AnimState:PlayAnimation("hit")
    inst.AnimState:PushAnimation("idle", false)
end

local function OnIgniteFn(inst)
    if inst.components.childspawner ~= nil then
        inst.components.childspawner:ReleaseAllChildren()
    end
end

local function ongohome(inst, child)
    if child.components.inventory ~= nil then
        for k,v in pairs(child.components.inventory:ReferenceAllItems()) do
            if v.prefab then
                inst.components.container:GiveItem(v)
            end
        end
    end
end

local function onspawned(inst, child)
    child:AddTag("noharass")
    for k,v in pairs(inst.components.container:GetItemsWithTag("fertilizer")) do
        if not v:HasTag("fertilizer_oceanic") then
            child.components.inventory:GiveItem(v)
        end
    end
end

local function onbuilt(inst)
    inst.AnimState:PlayAnimation("place")
    inst.AnimState:PushAnimation("idle")
    inst.SoundEmitter:PlaySound("ia/common/monkey_barrel_place")
end

local INTERACT_NO_TAGS = {"INLIMBO", "NOCLICK", "irreplaceable", "knockbackdelayinteraction", "event_trigger", "minesprung", "mineactive", "catchable", "fire", "light", "spider",
    "cursed", "paired", "bundle", "heatrock", "deploykititem", "boatbuilder", "singingshell", "archive_lockbox", "simplebook", "furnituredecor", "flower", "gemsocket", "structure",
    "donotautopick", "outofreach", "_container", "nosteal", "heavy", "_health"}
local INTERACT_ONEOF_TAGS = {"pickable", "readyforharvest", "barren", "_inventoryitem"}

local function gatheroffscreen(inst)
    if not inst:IsAsleep() or not (inst.components.childspawner.childreninside == inst.components.childspawner.maxchildren) then
        return
    end

    local x, y, z = inst.Transform:GetWorldPosition()

    local simulation_ent = CreateEntity()

    simulation_ent:AddComponent("inventory")

    for k,v in pairs(TheSim:FindEntities(x, y, z, 20, --[[prime apes' max wander dist]] nil, INTERACT_NO_TAGS, INTERACT_ONEOF_TAGS)) do
        if (v.components.pickable and v.components.pickable.caninteractwith) and v:IsOnPassablePoint() then
            if not v.components.pickable:CanBePicked() then
                local volcanic = (v.components.witherable and v.components.witherable.volcanic) or false -- volcanic is nil if the pickable isn't volcanic, but it needs to be
                -- false so it's equal to item:HasTag("fertilizer_volcanic") if the plant isn't volcanic and the fertilizer isn't volcanic or the plant is volcanic and the
                -- fertilizer is volcanic
                local fertilizer = inst.components.container:FindItem(function(item)
                    return item:HasTag("fertilizer") and not item:HasTag("fertilizer_oceanic") and volcanic == item:HasTag("fertilizer_volcanic")
                end)
                if fertilizer then
                    if fertilizer.components.stackable then
                        fertilizer.components.stackable:Get(1):Remove()
                    else
                        fertilizer:Remove()
                    end
                    v.components.pickable:Fertilize(fertilizer, inst)
                end
            else
                v.components.pickable:Pick(simulation_ent)
            end
        elseif v.components.crop and v.components.crop:IsReadyForHarvest() and v:IsOnPassablePoint() then
            v.components.crop:Harvest(simulation_ent)
        elseif v.components.inventoryitem then
            inst.components.container:GiveItem(v)
        end
    end

    for k,v in pairs(simulation_ent.components.inventory:ReferenceAllItems()) do
        local saltbox = FindEntity(inst, 20, NotFull, {"saltbox"})
        local icebox = FindEntity(inst, 20, NotFull, {"fridge"})
        local container = inst.components.container:IsFull() and FindEntity(inst, 20, function(container)
            return NotFull(container) and (not container.components.container.itemtestfn or simulation_ent.components.inventory:FindItem(function(item)
                return container.components.container:itemtestfn(item)
            end))
        end, {"_container"})
        for k,container in pairs({saltbox, icebox, container or inst}) do
            for k,v in pairs(container.components.container.itemtestfn and simulation_ent.components.inventory:FindItems(saltbox.components.container.itemtestfn or simulation_ent.components.inventory:ReferenceAllItems())) do
                v.components.inventoryitem:RemoveFromOwner(true)
                inst.components.container:GiveItem(v)
            end
        end
    end

    simulation_ent:Remove()
end

local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddMiniMapEntity()
    inst.entity:AddNetwork()

    inst.MiniMapEntity:SetIcon("prime_ape.tex")

    inst:AddTag("foodpreserver")
    inst:AddTag("nocool")

    MakeObstaclePhysics(inst, 1)

    inst.AnimState:SetBank("barrel_wilbur")
    inst.AnimState:SetBuild("monkey_barrel_wilbur")
    inst.AnimState:PlayAnimation("idle", true)

    -- MakeSnowCoveredPristine(inst)

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    inst:AddComponent("container")
    inst.components.container:WidgetSetup("primeapebarrel_wilbur")
    inst.components.container.skipclosesnd = true
    inst.components.container.skipopensnd = true

    inst:AddComponent("childspawner")
    inst.components.childspawner:SetRegenPeriod(TUNING.PRIMEAPE_HUT_REGEN_PERIOD)
    inst.components.childspawner:SetSpawnPeriod(TUNING.PRIMEAPE_HUT_SPAWN_PERIOD)
    inst.components.childspawner:SetMaxChildren(4)

    inst.components.childspawner:StartRegen()
    inst.components.childspawner.childname = "primeape"
    inst.components.childspawner:StartSpawning()
    inst.components.childspawner.ongohome = ongohome
    inst.components.childspawner:SetSpawnedFn(onspawned)
    inst.components.childspawner.allowboats = true

    inst:AddComponent("lootdropper")
    inst.components.lootdropper:SetChanceLootTable("primeapebarrel")

    inst:AddComponent("workable")
    inst.components.workable:SetWorkAction(ACTIONS.HAMMER)
    inst.components.workable:SetWorkLeft(4)
    inst.components.workable:SetOnFinishCallback(onhammered)
    inst.components.workable:SetOnWorkCallback(onhit)

    -- inst:ListenForEvent("onbuilt", onbuilt)

    inst:AddComponent("inspectable")

    inst:AddComponent("hauntable")
    inst.components.hauntable:SetHauntValue(TUNING.HAUNT_SMALL)

    MakeLargeBurnable(inst)
    MakeLargePropagator(inst)

    inst:ListenForEvent("onignite", OnIgniteFn)

    inst:DoPeriodicTask(TUNING.SEG_TIME * 3, gatheroffscreen)

	return inst
end

return Prefab( "primeapebarrel_wilbur", fn, assets, prefabs),
MakePlacer("primeapebarrel_wilbur_placer", "barrel_wilbur", "monkey_barrel_wilbur", "idle")
