local assets =
{
    Asset("ANIM", "anim/ia_slingshotammo.zip"),
}


-- temp aggro system for the slingshots
local function no_aggro(attacker, target)
	local targets_target = target.components.combat ~= nil and target.components.combat.target or nil
	return targets_target ~= nil and targets_target:IsValid() and targets_target ~= attacker and attacker ~= nil and attacker:IsValid()
			and (GetTime() - target.components.combat.lastwasattackedbytargettime) < 4
			and (targets_target.components.health ~= nil and not targets_target.components.health:IsDead())
end

local function ImpactFx(inst, attacker, target)
    if target ~= nil and target:IsValid() then
		local impactfx = SpawnPrefab(inst.ammo_def.impactfx)
        impactfx.Transform:SetPosition(target.Transform:GetWorldPosition())
    end
end

local function OnAttack(inst, attacker, target)
	if target ~= nil and target:IsValid() and attacker ~= nil and attacker:IsValid() then
		if inst.ammo_def ~= nil and inst.ammo_def.onhit ~= nil then
			inst.ammo_def.onhit(inst, attacker, target)
		end
		ImpactFx(inst, attacker, target)
	end
end

local function OnPreHit(inst, attacker, target)
    if target ~= nil and target:IsValid() and target.components.combat ~= nil and no_aggro(attacker, target) then
        target.components.combat:SetShouldAvoidAggro(attacker)
	end

    if inst.ammo_def and inst.ammo_def.onprehit then
        inst.ammo_def.onprehit(inst, attacker, target)
    end
end

local function OnHit(inst, attacker, target)
    if target ~= nil and target:IsValid() and target.components.combat ~= nil then
		target.components.combat:RemoveShouldAvoidAggro(attacker)
	end
    inst:Remove()
end

local function NoHoles(pt)
    return not TheWorld.Map:IsPointNearHole(pt)
end

local function OnHit_Coconut(inst, attacker, target)
    inst.components.lootdropper:SpawnLootPrefab("coconut_halved")
    inst.components.lootdropper:SpawnLootPrefab("coconut_halved")
end

local function OnPreHit_Limestone(inst, attacker, target)
    if target and target:IsValid() and target:IsOnOcean() then
        inst.components.weapon:SetDamage(TUNING.SLINGSHOT_AMMO_DAMAGE_LIMESTONE_WATER)
    end
end

local function OnHit_Obsidian(inst, attacker, target)
    if target and target:IsValid() and target.components.burnable then
        target.components.burnable:Ignite(nil, attacker)
    end

    inst:Remove()
end

local function OnHit_Tar(inst, attacker, target)
	local debuffkey = inst.prefab

	if target ~= nil and target:IsValid() then
        target:PushEvent("attacked", { attacker = attacker, damage = 0, weapon = inst })

        if target._slingshot_tartask ~= nil then
            target._slingshot_tartask:Cancel()
        else
            target.components.combat:AddPeriodModifier(debuffkey, TUNING.SLINGSHOT_AMMO_TAR_ATTACKSPEED_MULT)
            target._previous_multcolour = target.AnimState:GetMultColour()
            target.AnimState:SetMultColour(target._previous_multcolour * 0.5, target._previous_multcolour * 0.5, target._previous_multcolour * 0.5, 1)
            target.components.health.externalfiredamagemultipliers:SetModifier(debuffkey, TUNING.SLINGSHOT_AMMO_TAR_FIREDAMAGE_MULT)
            if target.components.locomotor then
                target.components.locomotor:SetExternalSpeedMultiplier(target, debuffkey, TUNING.SLINGSHOT_AMMO_TAR_MOVESPEED_MULT)
            end
            if target.components.burnable and not target._slingshot_tarextendburningtask then
                if target.components.burnable:IsBurning() then
                    target.components.burnable:ExtendBurning()
                end
                target._slingshot_tarextendburningtask = target:DoPeriodicTask(target.components.burnable.burntime - 0.1, function(inst)
                    if inst.components.burnable:IsBurning() then
                        inst.components.burnable:ExtendBurning()
                    end
                end)
            end
        end

        target._slingshot_tartask = target:DoTaskInTime(TUNING.SLINGSHOT_AMMO_TAR_DURATION, function(inst)
            inst.components.health.externalfiredamagemultipliers:RemoveModifier(debuffkey)
            inst.components.combat:RemovePeriodModifier(debuffkey)
            inst.AnimState:SetMultColour(inst._previous_multcolour, inst._previous_multcolour, inst._previous_multcolour, 1)
            inst._previous_multcolour = nil
            if target.components.locomotor then
                target.components.locomotor:RemoveExternalSpeedMultiplier(target, debuffkey)
            end
            if target._slingshot_tarextendburningtask then
                target._slingshot_tarextendburningtask:Cancel()
                target._slingshot_tarextendburningtask = nil
                target.components.burnable:Extinguish()
            end
            inst._slingshot_tartask:Cancel()
            inst._slingshot_tartask = nil
        end)
	end
end

local function OnMiss(inst, owner, target)
    inst:Remove()
end

local function projectile_fn(ammo_def)
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddNetwork()

    inst.Transform:SetFourFaced()

    MakeProjectilePhysics(inst)

    inst.AnimState:SetBank("slingshotammo")
    inst.AnimState:SetBuild("ia_slingshotammo")
    inst.AnimState:PlayAnimation("spin_loop", true)
	if ammo_def.symbol ~= nil then
		inst.AnimState:OverrideSymbol("rock", "ia_slingshotammo", ammo_def.symbol)
	end

    --projectile (from projectile component) added to pristine state for optimization
    inst:AddTag("projectile")

	if ammo_def.tags then
		for _, tag in pairs(ammo_def.tags) do
			inst:AddTag(tag)
		end
	end

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    inst.persists = false

	inst.ammo_def = ammo_def

	inst:AddComponent("weapon")
	inst.components.weapon:SetDamage(ammo_def.damage)
	inst.components.weapon:SetOnAttack(OnAttack)

    inst:AddComponent("projectile")
    inst.components.projectile:SetSpeed(25)
    inst.components.projectile:SetHoming(false)
    inst.components.projectile:SetHitDist(1.5)
    inst.components.projectile:SetOnPreHitFn(OnPreHit)
    inst.components.projectile:SetOnHitFn(OnHit)
    inst.components.projectile:SetOnMissFn(OnMiss)
    inst.components.projectile.range = 30
	inst.components.projectile.has_damage_set = true

    if inst.ammo_def.name == "coconut" then
        inst:AddComponent("lootdropper")
    end

    return inst
end

local function inv_fn(ammo_def)
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddNetwork()

    MakeInventoryPhysics(inst)

    inst.AnimState:SetRayTestOnBB(true)
    inst.AnimState:SetBank("slingshotammo")
    inst.AnimState:SetBuild("ia_slingshotammo")
    inst.AnimState:PlayAnimation("idle")
	if ammo_def.symbol ~= nil then
		inst.AnimState:OverrideSymbol("rock", "ia_slingshotammo", ammo_def.symbol)
        -- inst.scrapbook_overridedata = {"rock", "ia_slingshotammo", ammo_def.symbol}
	end

    inst:AddTag("molebait")
	inst:AddTag("slingshotammo")
	inst:AddTag("reloaditem_ammo")

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

	inst:AddComponent("reloaditem")

    inst:AddComponent("edible")
    inst.components.edible.foodtype = FOODTYPE.ELEMENTAL
    inst.components.edible.hungervalue = 1
    inst:AddComponent("tradable")

    inst:AddComponent("stackable")
    inst.components.stackable.maxsize = TUNING.STACK_SIZE_TINYITEM

    inst:AddComponent("inspectable")

    inst:AddComponent("inventoryitem")
    inst.components.inventoryitem:SetSinks(true)

    inst:AddComponent("bait")
    MakeHauntableLaunch(inst)

	if ammo_def.fuelvalue ~= nil then
		inst:AddComponent("fuel")
		inst.components.fuel.fuelvalue = ammo_def.fuelvalue
	end

	if ammo_def.onloadammo ~= nil and ammo_def.onunloadammo ~= nil then
		inst:ListenForEvent("ammoloaded", ammo_def.onloadammo)
		inst:ListenForEvent("ammounloaded", ammo_def.onunloadammo)
		inst:ListenForEvent("onremove", ammo_def.onunloadammo)
	end

    return inst
end

local function hitfx_fn(ammo_def)
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddNetwork()

    inst.AnimState:SetBank("slingshotammo")
    inst.AnimState:SetBuild("ia_slingshotammo")
    inst.AnimState:PlayAnimation("used")
    inst.AnimState:SetRayTestOnBB(true)
    inst.AnimState:SetFinalOffset(FINALOFFSET_MAX)
    inst.AnimState:OverrideSymbol("rock", "ia_slingshotammo", ammo_def.symbol)

    inst:AddTag("FX")

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    inst.SoundEmitter:PlaySound(ammo_def.sound)

    inst:ListenForEvent("animover", inst.Remove)

    inst.persists = false

    return inst
end

local ammo =
{
    {
        name = "coconut",
        no_inv_item = true,
		symbol = "gold",
        damage = TUNING.SLINGSHOT_AMMO_DAMAGE_COCONUT,
        onhit = OnHit_Coconut,
        hitfx_name = "coconut",
        sound = "ia/common/bamboo_hack"
    },
	{
		name = "slingshotammo_limestone",
		symbol = "marble",
		damage = TUNING.SLINGSHOT_AMMO_DAMAGE_LIMESTONE,
        onprehit = OnPreHit_Limestone,
        hitfx_name = "limestone",
        sound = "dontstarve/characters/walter/slingshot/rock"
	},
	{
		name = "slingshotammo_obsidian", 
		symbol = "thulecite",
		damage = TUNING.SLINGSHOT_AMMO_DAMAGE_OBSIDIAN,
		onhit = OnHit_Obsidian,
        hitfx_name = "obsidian",
        sound = "dontstarve/characters/walter/slingshot/thulecite"
	},
	{
		name = "slingshotammo_tar", 
		symbol = "poop",
		damage = nil,
        fuelvalue = TUNING.LARGE_FUEL / 10, -- 1/10th the value of using tar
		onhit = OnHit_Tar,
        hitfx_name = "tar",
        sound = "dontstarve/characters/walter/slingshot/poop"
	}

}

local ammo_prefabs = {}
for _, v in ipairs(ammo) do
	v.impactfx = "slingshotammo_hitfx_" .. (v.hitfx_name or "rock")

	---
	if not v.no_inv_item then
		table.insert(ammo_prefabs, Prefab(v.name, function() return inv_fn(v) end, assets))
	end

    table.insert(ammo_prefabs, Prefab("slingshotammo_hitfx_"..v.hitfx_name, function() return hitfx_fn(v) end, assets))

	local prefabs =
	{
		"shatter",
	}
	table.insert(prefabs, v.impactfx)
	table.insert(ammo_prefabs, Prefab(v.name.."_proj", function() return projectile_fn(v) end, assets, prefabs))
end

return unpack(ammo_prefabs)