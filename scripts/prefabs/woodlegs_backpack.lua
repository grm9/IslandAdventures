local assets = {
    Asset("ANIM", "anim/swap_pirate_booty_bag.zip"),
}

local function canthrowatpointfn(inst)
    for i=9,12,1 do
        if inst.components.container:GetItemInSlot(i) then
            return true
        end
    end

    return false
end

local function onthrowfn(inst, projectile)
    -- local owner = inst.components.inventoryitem:GetGrandOwner()
    -- local angle = owner.Transform:GetRotation()*(PI / 180)
    -- local pos = owner:GetPosition() + Vector3(-math.cos(angle), 0, 3 * math.sin(angle))

    -- owner:ForceFacePoint(pos)
    local owner = inst.components.inventoryitem:GetGrandOwner()
    owner.sg:GoToState("hit_spike", "tall")

    for i=9,12,1 do
        local item = inst.components.container:GetItemInSlot(i)
        if item then
            if item:HasTag("crumblingcoconade") then
                local x, y, z = owner.Transform:GetWorldPosition()
                local sinkhole = SpawnPrefab("eyeofterror_sinkhole")
                sinkhole.Transform:SetPosition(x, y, z)
                sinkhole:PushEvent("docollapse")
                SpawnPrefab("explode_large").Transform:SetPosition(x, y, z)
                SpawnPrefab("explodering_fx").Transform:SetPosition(x, y, z)
                for k, v in ipairs(AllPlayers) do
                    local distSq = v:GetDistanceSqToInst(owner)
                    local k = math.max(0, math.min(1, distSq / 400))
                    local intensity = k * 0.75 * (k - 2) + 0.75
                    if intensity > 0 then
                        v:ScreenFlash(intensity)
                        v:ShakeCamera(CAMERASHAKE.FULL, .7, .02, intensity / 2)
                    end
                end
            end
            projectile.components.explosive.explosiverange = item.components.explosive and item.components.explosive.explosiverange or 3
            projectile.components.explosive.explosivedamage = item.components.explosive and item.components.explosive.explosivedamage * 2 or 100
            if item.components.stackable and item.components.stackable.stacksize > 1 then
                item.components.stackable:SetStackSize(item.components.stackable.stacksize - 1)
            else
                item:Remove()
            end
            break
        end
    end
end

local function onequip(inst, owner)
    owner.AnimState:OverrideSymbol("swap_body", "swap_pirate_booty_bag", "backpack")
    owner.AnimState:OverrideSymbol("swap_body", "swap_pirate_booty_bag", "swap_body")

    if inst.components.container ~= nil then
        inst.components.container:Open(owner)
    end

    inst.components.coconadegenerator:Start(owner)
end

local function onunequip(inst, owner)
    owner.AnimState:ClearOverrideSymbol("swap_body")
    owner.AnimState:ClearOverrideSymbol("backpack")

    if inst.components.container ~= nil then
        inst.components.container:Close(owner)
    end

    inst.components.coconadegenerator:Stop(owner)
end

local function onequiptomodel(inst, owner, from_ground)
    if inst.components.container ~= nil then
        inst.components.container:Close(owner)
    end

    if inst.coconade_task then
        inst.coconade_task:Cancel()
        inst.coconade_task = nil
    end
end

--[[ local function onburnt(inst)
    if inst.components.container ~= nil then
        inst.components.container:DropEverything()
        inst.components.container:Close()
    end

    SpawnPrefab("ash").Transform:SetPosition(inst.Transform:GetWorldPosition())

    inst:Remove()
 end

local function onignite(inst)
    if inst.components.container ~= nil then
        inst.components.container.canbeopened = false
    end
end

local function onextinguish(inst)
    if inst.components.container ~= nil then
        inst.components.container.canbeopened = true
    end
end]]

local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddMiniMapEntity()
    inst.entity:AddNetwork()

    MakeInventoryPhysics(inst)

    inst.AnimState:SetBank("pirate_booty_bag")
    inst.AnimState:SetBuild("swap_pirate_booty_bag")
    inst.AnimState:PlayAnimation("anim")

    inst:AddTag("backpack")

    inst.MiniMapEntity:SetIcon("piratepack.tex")

    inst.foleysound = "ia/common/foley/pirate_booty_pack"

	MakeInventoryFloatable(inst)
	inst.components.floater:UpdateAnimations("idle_water", "anim")

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    inst:AddComponent("inspectable")

    inst:AddComponent("inventoryitem")
    inst.components.inventoryitem.cangoincontainer = false

    inst:AddComponent("equippable")
    inst.components.equippable.equipslot = EQUIPSLOTS.BACK

    inst.components.equippable:SetOnEquip(onequip)
    inst.components.equippable:SetOnUnequip(onunequip)
    inst.components.equippable:SetOnEquipToModel(onequiptomodel)

    inst:AddComponent("container")
    inst.components.container:WidgetSetup("woodlegs_backpack")
    -- inst:ListenForEvent("itemlose", function(inst, data)
    --     if data.slot and data.slot > 8 and data.prev_item then
    --         data.prev_item.Transform:SetPosition(inst.Transform:GetWorldPosition())
    --         data.prev_item:DoTaskInTime(0, function(item)
    --             if item and item:IsValid() then
    --                 if not item.components.explosive then
    --                     item:AddComponent("explosive")
    --                 end
    --                 item.components.explosive:OnBurnt()
    --             end
    --         end)
    --     end
    -- end)

    inst:AddComponent("thrower")
    inst.components.thrower.canthrowatpointfn = canthrowatpointfn
    inst.components.thrower.throwable_prefab = "woodlegs_cannonshot"
    inst.components.thrower.onthrowfn = onthrowfn

    inst:AddComponent("coconadegenerator")

    MakeHauntableLaunchAndDropFirstItem(inst)
    inst.components.hauntable.hauntvalue = TUNING.HAUNT_LARGE

    return inst
end

return Prefab("woodlegs_backpack", fn, assets)
