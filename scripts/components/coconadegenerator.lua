local function onchangedtimeleft(self, timeleft)
    if timeleft <= 0 then
        self.timeleft = self.cooldown
        self:SpawnCoconade()
    end
end

local function TimeLeftOnPeriod(inst)
    inst.components.coconadegenerator:DecreaseTimeLeft()
end

local function TimeLeftOnAttack(inst, data)
    inst.components.coconadegenerator:DecreaseTimeLeftAttack(data)
end

local CoconadeGenerator = Class(function(self, inst)
    self.inst = inst
    self.cooldown = TUNING.SEG_TIME
end, nil, {
    timeleft = onchangedtimeleft
})

function CoconadeGenerator:Start(owner)
    local inst = self.inst

    self.timeleft = self.cooldown
    inst.coconade_task = inst:DoPeriodicTask(1, TimeLeftOnPeriod)
    owner.coconadegenerator_onattackother = function(_, data)
        self:DecreaseTimeLeftAttack(data)
    end
    owner:ListenForEvent("onattackother", owner.coconadegenerator_onattackother)
end

function CoconadeGenerator:Stop(owner)
    local inst = self.inst

    inst.coconade_task:Cancel()
    inst.coconade_task = nil
    owner:RemoveEventCallback("onattackother", owner.coconadegenerator_onattackother)
    owner.coconadegenerator_onattackother = nil
end

function CoconadeGenerator:DecreaseTimeLeft()
    local unavailableslots = 0
    for i=9,12,1 do
        if self.inst.components.container:GetItemInSlot(i) then
            unavailableslots = unavailableslots + 1
        end
    end

    if unavailableslots == 4 then
        self.notimeleftdecrease = true
    else
        self.timeleft = self.timeleft - (1 / (unavailableslots + 1))
        self.notimeleftdecrease = nil
    end
end

function CoconadeGenerator:DecreaseTimeLeftAttack(data)
    local target = data.target
    local decrease

    if self.notimeleftdecrease or not data.target then
        return
    end

    if data.target:HasTag("smallcreature") or data.target:HasTag("canbetrapped") then
        decrease = 0.25
    elseif data.target:HasTag("epic") or data.target:HasTag("largecreature") then
        decrease = 1
    else
        decrease = 0.5
    end

    self.timeleft = self.timeleft - decrease
end

function CoconadeGenerator:SpawnCoconade()
    for i=9,12,1 do
        if not self.inst.components.container:GetItemInSlot(i) then
            local coconade = SpawnPrefab("coconade")
            self.inst.components.container:GiveItem(coconade, i)
            coconade:TurnCrumbling()
            break
        end
    end
end

return CoconadeGenerator