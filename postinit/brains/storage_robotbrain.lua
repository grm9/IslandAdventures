local IAENV = env
GLOBAL.setfenv(1, GLOBAL)
--------------------------------------------------------------

local StorageRobotBrain = require("brains/storage_robotbrain")

local NOTAGS = {"ia_boat"}
local _CONTAINER_CANT_TAGS = UpvalueHacker.GetUpvalue(StorageRobotBrain.OnStart, "StoreItemAction", "FindContainerWithItem", "CONTAINER_CANT_TAGS")

for i,tag in pairs(NOTAGS) do
    table.insert(_CONTAINER_CANT_TAGS, tag)
end