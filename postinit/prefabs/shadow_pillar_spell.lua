local IAENV = env
GLOBAL.setfenv(1, GLOBAL)

IAENV.AddPrefabPostInit("shadow_pillar_spell", function(inst) -- this code is atrocious and idk how to make it better, it'll need to be updated if shadow pillar code gets changed
-- so remember to update it after maxwell skill tree if it'll change shadow pillars
    if not TheWorld.ismastersim then
        return
    end
    local firstperiodictask
    for k,v in pairs(inst.pendingtasks) do
        if k.period == 0.25 then
            firstperiodictask = k
            break
        end
    end
    local fn_Old = firstperiodictask.fn
    firstperiodictask.fn = function(inst, targets, newpillars, ...)
        local map = TheWorld.Map
        local caster = inst.caster ~= nil and inst.caster:IsValid() and inst.caster or nil
        local castercombat = caster ~= nil and caster.components.combat or nil
        local item = inst.item ~= nil and inst.item:IsValid() and inst.item or nil
        local x, y, z = inst.Transform:GetWorldPosition()
        local ents = TheSim:FindEntities(x, 0, z, UpvalueHacker.GetUpvalue(fn_Old, "AOE_RADIUS"), {"doydoy"}, {"INLIMBO"})
        for i, v in ipairs(ents) do
            if v ~= caster and not targets[v] and v.entity:IsVisible() and
                not (v.components.health ~= nil and v.components.health:IsDead()) and
                v.prefab == "doydoy" then
                x, y, z = v.Transform:GetWorldPosition()
                if map:IsPassableAtPoint(x, y, z, true) then
                    targets[v] = true
                    UpvalueHacker.GetUpvalue(fn_Old, "DoPillarsTarget")(v, caster, item, newpillars, map, x, z)
                end
            end
        end

        return fn_Old(inst, targets, newpillars, ...)
    end
end)