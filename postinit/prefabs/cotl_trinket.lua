local IAENV = env
GLOBAL.setfenv(1, GLOBAL)

----------------------------------------------------------------------------------------
--Try to initialise all functions locally outside of the post-init so they exist in RAM only once
----------------------------------------------------------------------------------------

IAENV.AddPrefabPostInit("cotl_trinket", function(inst)
    if TheWorld.ismastersim then
        inst.components.tradable.dubloonvalue = TUNING.DUBLOON_VALUES.COTL_TRINKET
    end
end)
