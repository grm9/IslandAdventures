@ECHO off

SET "SHADERCOMPILER=C:\\Program Files (x86)\\Steam\\steamapps\\common\\Don't Starve Mod Tools\\mod_tools\\tools\\bin\\ShaderCompiler.exe"
SET "SHADEROUTPUT=..\\shaders\\"

CALL :BuildShader floodstate tilestate

PAUSE
GOTO :eof

:BuildShader
ECHO Building %1.ksh...
"%SHADERCOMPILER%" -little %1 %2.vs %1.ps %SHADEROUTPUT%%1.ksh -oglsl
GOTO :eof